﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public int n;
        public int[] P;
        public TextBox[] textbox;
        int g = 0;
        int game = 0;
        int error = 0;
        Random Rand = new Random();

        public Form1()
        {
            InitializeComponent();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)//ilustrated help
        {
            Form3 V = new Form3();
            V.Show();
        }

        private void finish()//ilustrated finish form
        {
            //creating_sets();
            Form2 V = new Form2(this);
            V.print_message((g)  % 2, game);
            V.Show();
           // g++;
        }

         public void creating_sets()//создание множеств
         {
            int n = 3;
            label3.Text = "Введите значение";
            g = 0;
            
            textbox = new TextBox [n];
            for (int i = 0; i < 3; i++)
            {
                textbox[i] = new TextBox();
            }
            textbox[0] = textBox4;
            textbox[1] = textBox5;
            textbox[2] = textBox6;
            P = new int[n];
            for (int i = 0; i < n; i++)
            {
                P [i] = Rand.Next(10)+1; //инициализация множеств
                textbox[i].Text = P[i].ToString();
            }
         }
        /// <summary>
        /// изменяет значение кучек
        /// </summary>
        /// <param name="k">количество спичек, на которое меняем кучку</param>
        /// <param name="i">номер кучки, которую меняем</param>
        private void changing_values(int k, int i)
        {
            if (k > P[i])
            {
                label3.Text = @"Вы ввели слишком большое число,
пожалуйста введите число не превышающее 
количество оставшихся спичек или 
выберите другую кучку!";
                error = 1;
            }
            else
            {
                this.P[i] = P[i] - k;
                textbox[i].Text = P[i].ToString();
            }
            int s = 0;
            for (int j = 0; j < 3; j++)
            {
                s += P[j];
            }
            if (s == 0)
            {
                Form1 cl = new Form1();                
                finish();
            }
        }

        private void ReadText() //чтение элементов из формы1
        {
            TextBox[] text_box = { textBox1, textBox2, textBox3 };
            int N = 0;
            int k = 0;
                for (int i = 0; i < 3; i++)
                {
                    N = Convert.ToInt32(text_box[i].Text);
                    if (N != 0)
                    {
                        k = i;
                        break;
                    }
                }
                if (N == 0)
                {
                    label3.Text = "Вы не ввели значение!";
                    error = 1;
                }
                else
                {
                    label3.Text = "";
                    changing_values(N, k);
                }
            k = 0;
            for (int i = 0; i < 3; i++)
            {
                text_box[i].Text = k.ToString();
            }           
        }

        private void FindValuesComputer()//ищет значение, которое будет изменять компьтер
        {
            int k = 0;
            int[] summ = new int[3];
            summ[0] = P[0] + P[1];
            summ[1] = P[1] + P[2];
            summ[2] = P[2] + P[0];
            int min_summ = summ[0];
            for (int i = 0; i < 3; i++)
            {
                if (summ[i] > min_summ)
                    continue;
                else
                {
                    min_summ = summ[i];
                    k = i;
                }
            }
            int m = 0;
            switch (k)
            {
                case 0:
                    if (P[2] <= summ[0])
                    {
                        m = P[2];
                        P[2] = 0;
                    }
                    else
                    {
                        m = P[2] - summ[0];
                        P[2] -= m;
                    }
                        textbox[2].Text = P[2].ToString();
                    label3.Text = "Изменил значение 3 кучку на " + m + "спичек";
                    break;
                case 1:
                    if (P[0] <= summ[1])
                    {
                        m = P[0];
                        P[0] = 0;
                    }
                    else
                    {
                        m = P[0] - summ[1];
                        P[0] -= m;
                    }
                        textbox[0].Text = P[0].ToString();
                    label3.Text = "Изменил значение 1 кучку на " + m + "спичек";
                    break;
                case 2:
                    if (P[1] <= summ[2])
                    {
                        m = P[1];
                        P[1] = 0;
                    }
                    else
                    {
                        m = P[1] - summ[2];
                        P[1] -= m;
                    }
                        textbox[1].Text = P[1].ToString();
                    label3.Text = "Изменил значение 2 кучку на " + m + "спичек";
                    break;
            }
            int s = 0;
            for (int j = 0; j < 3; j++)
            {
                s += P[j];
            }
            if (s == 0)
            {
                Form1 cl = new Form1();
                finish();
            }
            g++;
            //MessageBox.Show("AI g = " + g.ToString());
        }

        private void Выполнить_Click(object sender, EventArgs e)
        {
            if (game == 1)
            {
                ReadText();
                if (error == 0)
                {
                    g++;
                    //MessageBox.Show("Player g = " + g.ToString());
                    FindValuesComputer();
                }
                
                error = 0;
            }
            if (game == 2)
            {
                ReadText();
                if (error == 0)
                {
                    g++;
                   // MessageBox.Show("Player g = " + g.ToString());
                    if (g % 2 == 0)
                        label3.Text = "Ходит первый игрок";
                    else
                        label3.Text = "Ходит второй игрок";
                }
                error = 0;
            }
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            creating_sets();
            label3.Text = "Введите значение";
            g = 0;
        }

        private void computerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            creating_sets();
            label3.Text = "Введите значение";
            game = 1;
        }

        private void peopleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            creating_sets();
            label3.Text = "Введите значение";
            game = 2;
        }
    }
}