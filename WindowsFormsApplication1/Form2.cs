﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        Form1 f;
        public Form2(Form1 f)
        {
            InitializeComponent();
            this.f = f;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            f.creating_sets();
            Close();
        }

        public void print_message(int gam, int game)
        {
            if (gam == 0 && game == 2)
                label1.Text = "Победил первый игрок! Позравляем!";
            if (gam == 1 && game == 2)
                label1.Text = "Победил второй игрок! Поздравляем!";
            if (gam == 0 && game == 1)
                label1.Text = "Вы победили! Поздравляем!";
            if (gam == 1 && game == 1)
                label1.Text = "Вы проиграли! Поздравляем!";
        }
    }
}
