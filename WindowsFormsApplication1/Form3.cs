﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            PrintText();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PrintText()
        {
            label1.Text = @"Математическая игра, в которой два игрока 
 по очереди берут предметы, разложенные на три кучки. 
 За один ход игроком может быть взято любое количество 
 предметов (большее нуля) из одной кучки. Выигрывает 
 игрок, взявший последний предмет.";
        }
    }
}
